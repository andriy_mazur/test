<?php

// $url = 'test.json';
$url = 'testWithBsonAtts.json';
// Specially added Mongo atts into the export file like as "updatedAt": ISODate("2015-09-29T13:03:13.0Z"), NumberInt(123), ObjectId("560aa32c17571e160c67sdb5a")
// to test how script will work with NON-valid JSON


// Here set path to json file
// In future I think we should split file selecting and parser into different files (parts)
// And show our error if file doesn't exist

date_default_timezone_set('Europe/Kiev');
$mydate = date('Y-m-d-H-i-s');
$filename = "";
$filename = $_SERVER['DOCUMENT_ROOT']."/test/".$mydate.".txt";
//Set filename for result's file (once for full json-file)


$new = array();
$key = 0;


$words = array_flip(["season", "width", "profile", "diameter", "et", "pcd", "hub"]);
// Flip array to make array_key_exists function workable


$file = fopen($url, 'r');

$mycount = findGoods($file, $words, $filename);

function deleteBsonAtts($str){

	return preg_replace( "/(\"\w{1,}\"\:\s)\w{1,}\((\"?[a-zA-Z0-9\.\-\:]{1,}\"?)\)/i", "$1$2", $str );

}

function findGoods($file, $words, $filename){

	$temp = "";
	$part = "";
	$mycount = 0;

	while(!feof($file)){
		$part = fgets($file);
		$part = deleteBsonAtts($part);
		$temp.= $part;
		if( strpos($part, "\"description\": null") != NULL ){
			$part = fgets($file);
			$part = deleteBsonAtts($part);
			$temp = "[" . $temp . $part ."]";
			$result = json_decode($temp);
			mywrite(getProperties($result, $words), $filename);
			$temp = "";
			$result = "";
			$mycount++;
		}
	}
	return $mycount;
}

function getProperties($result, $words){
	foreach ($result as $key => $value) {

		if(isset($value->model)&&!empty($value->model)){
			$new[$key]['model'] = $value->model;
		}

		if(isset($value->storePrice)){
			foreach ($value->storePrice as $key1 => $value1) {
				$new[$key]['price'] =  array_shift(get_object_vars($value1))[0]->price;
			}
		}

		if(isset($value->stock)){
			if($value->stock == 'inStock'){
				$new[$key]['availability'] = true;
			}
			else $new[$key]['availability'] = false;
		}

		if(isset($value->parameters)){
			foreach ($value->parameters as $key1 => $value1) {
				// array_search because array_key_exists search in keys, not in values
				if(array_key_exists($value1->typeName, $words)){

					$new[$key][$value1->typeName] = $value1->parameterValues[0]->name;
				}
			}
		}
	}
	return $new;
}

function mywrite($new, $filename){

	$resultstr = serialize($new);
	$handle = fopen($filename,'a') or die("can't open file");
	fwrite($handle,$resultstr);
	fwrite($handle, "\r\n");
	fclose($handle);
}

fclose($file);

echo "Path to results: ".$filename."\r\n".". Processed ".$mycount." goods";

?>